package envconf

import (
	"encoding"
	"os"
	"strconv"
	"strings"
	"testing"

	"gitlab.com/flimzy/testy"
)

type textUnmarshalerString string

var _ encoding.TextUnmarshaler = new(textUnmarshalerString)

func (s *textUnmarshalerString) UnmarshalText(text []byte) error {
	*s = textUnmarshalerString(strings.Trim(string(text), `"`))
	return nil
}

type textUnmarshalerInt int

var _ encoding.TextUnmarshaler = new(textUnmarshalerInt)

func (i *textUnmarshalerInt) UnmarshalText(text []byte) error {
	x, err := strconv.Atoi(string(text))
	if err != nil {
		return err
	}
	*i = textUnmarshalerInt(x)
	return nil
}

func TestReadConfig(t *testing.T) {
	type tst struct {
		dec      Decoder
		env      map[string]string
		target   interface{}
		expected interface{}
		err      string
	}
	tests := testy.NewTable()
	tests.Add("no config", func(t *testing.T) interface{} {
		type conf struct {
			Foo string
		}
		return tst{
			env:      map[string]string{},
			target:   &conf{},
			expected: &conf{},
		}
	})
	tests.Add("non pointer target", tst{
		target: int(1),
		err:    "dst must be a pointer",
	})
	tests.Add("non struct target", tst{
		target: new(int),
		err:    "dst must be a pointer to struct",
	})
	tests.Add("multi-pointer", func() interface{} {
		type conf struct {
			Foo string
		}
		c := &conf{}
		e := &conf{Foo: "abc"}
		return tst{
			target:   &c,
			env:      map[string]string{"FOO": "abc"},
			expected: &e,
		}
	})
	tests.Add("Infer exact field name", func() interface{} {
		type conf struct {
			FOO string
		}
		return tst{
			env: map[string]string{
				"FOO": "123",
			},
			target:   &conf{},
			expected: &conf{FOO: "123"},
		}
	})
	tests.Add("Infer case-insensitive field name", func() interface{} {
		type conf struct {
			Foo string
		}
		return tst{
			env: map[string]string{
				"FOO": "123",
			},
			target:   &conf{},
			expected: &conf{Foo: "123"},
		}
	})
	tests.Add("duplicate case-insisentive field names", func() interface{} {
		type conf struct {
			Foo string
			FOO string
		}
		return tst{
			env: map[string]string{
				"FOO": "123",
			},
			target:   &conf{},
			expected: &conf{Foo: "123", FOO: "123"},
		}
	})
	tests.Add("tag field names", func() interface{} {
		type conf struct {
			Bar string `conf:"FOO"`
		}
		return tst{
			env: map[string]string{
				"FOO": "123",
			},
			target:   &conf{},
			expected: &conf{Bar: "123"},
		}
	})
	tests.Add("text unmarshaler pointer", func() interface{} {
		type conf struct {
			Foo *textUnmarshalerString
			Bar *textUnmarshalerInt
		}
		expectedStr := textUnmarshalerString("123")
		expectedInt := textUnmarshalerInt(123)
		return tst{
			env: map[string]string{
				"FOO": `"123"`,
				"BAR": `123`,
			},
			target: &conf{},
			expected: &conf{
				Foo: &expectedStr,
				Bar: &expectedInt,
			},
		}
	})
	tests.Add("text unmarshaler concrete", func() interface{} {
		type conf struct {
			Foo textUnmarshalerString
			Bar textUnmarshalerInt
		}
		return tst{
			env: map[string]string{
				"FOO": `"123"`,
				"BAR": `123`,
			},
			target: &conf{},
			expected: &conf{
				Foo: textUnmarshalerString("123"),
				Bar: textUnmarshalerInt(123),
			},
		}
	})
	tests.Add("All simple types", func() interface{} {
		type conf struct {
			Bool    bool
			String  string
			Int     int
			Int8    int8
			Int16   int16
			Int32   int32
			Int64   int64
			Uint    uint
			Uint8   uint8
			Uint16  uint16
			Uint32  uint32
			Uint64  uint64
			Byte    byte
			Rune    rune
			Float32 float32
			Float64 float64
		}
		return tst{
			target: &conf{},
			env: map[string]string{
				"BOOL":    "true",
				"STRING":  "string",
				"INT":     "1",
				"INT8":    "2",
				"INT16":   "3",
				"INT32":   "4",
				"INT64":   "5",
				"UINT":    "6",
				"UINT8":   "7",
				"UINT16":  "8",
				"UINT32":  "9",
				"UINT64":  "10",
				"BYTE":    "'a'",
				"RUNE":    "'₹'",
				"FLOAT32": "1.0",
				"FLOAT64": "2.0",
			},
			expected: &conf{
				Bool:    true,
				String:  "string",
				Int:     1,
				Int8:    2,
				Int16:   3,
				Int32:   4,
				Int64:   5,
				Uint:    6,
				Uint8:   7,
				Uint16:  8,
				Uint32:  9,
				Uint64:  10,
				Byte:    'a',
				Rune:    '₹',
				Float32: 1.0,
				Float64: 2.0,
			},
		}
	})
	tests.Add("Pointers to simple types", func() interface{} {
		type conf struct {
			Bool    *bool
			String  *string
			Int     *int
			Int8    *int8
			Int16   *int16
			Int64   *int64
			Int32   *int32
			Uint    *uint
			Uint8   *uint8
			Uint16  *uint16
			Uint32  *uint32
			Uint64  *uint64
			Byte    *byte
			Rune    *rune
			Float32 *float32
			Float64 *float64
		}
		return tst{
			target: &conf{},
			env: map[string]string{
				"BOOL":    "true",
				"STRING":  "string",
				"INT":     "1",
				"INT8":    "2",
				"INT16":   "3",
				"INT32":   "4",
				"INT64":   "5",
				"UINT":    "6",
				"UINT8":   "7",
				"UINT16":  "8",
				"UINT32":  "9",
				"UINT64":  "10",
				"BYTE":    "'a'",
				"RUNE":    "'₹'",
				"FLOAT32": "1.0",
				"FLOAT64": "2.0",
			},
			expected: &conf{
				Bool:    &[]bool{true}[0],
				String:  &[]string{"string"}[0],
				Int:     &[]int{1}[0],
				Int8:    &[]int8{2}[0],
				Int16:   &[]int16{3}[0],
				Int32:   &[]int32{4}[0],
				Int64:   &[]int64{5}[0],
				Uint:    &[]uint{6}[0],
				Uint8:   &[]uint8{7}[0],
				Uint16:  &[]uint16{8}[0],
				Uint32:  &[]uint32{9}[0],
				Uint64:  &[]uint64{10}[0],
				Byte:    &[]byte{'a'}[0],
				Rune:    &[]rune{'₹'}[0],
				Float32: &[]float32{1.0}[0],
				Float64: &[]float64{2.0}[0],
			},
		}
	})
	tests.Add("bool false", func() interface{} {
		type conf struct {
			Bool *bool
		}
		return tst{
			target: &conf{},
			env: map[string]string{
				"BOOL": "F",
			},
			expected: &conf{
				Bool: &[]bool{false}[0],
			},
		}
	})
	tests.Add("blank bool", func() interface{} {
		type conf struct {
			Bool *bool
		}
		return tst{
			target: &conf{},
			env: map[string]string{
				"BOOL": "",
			},
			expected: &conf{
				Bool: &[]bool{false}[0],
			},
		}
	})
	tests.Add("bool error", func() interface{} {
		type conf struct {
			Foo *bool
		}
		return tst{
			target: &conf{},
			env: map[string]string{
				"FOO": "pink",
			},
			err: "Foo: Invalid value 'pink' for boolean",
		}
	})
	tests.Add("rune error", func() interface{} {
		type conf struct {
			Foo rune
		}
		return tst{
			target: &conf{},
			env: map[string]string{
				"FOO": "'pink'",
			},
			err: "Foo: Invalid value 'pink' for rune",
		}
	})
	tests.Add("byte error", func() interface{} {
		type conf struct {
			Foo byte
		}
		return tst{
			target: &conf{},
			env: map[string]string{
				"FOO": "'pink'",
			},
			err: "Foo: Invalid value 'pink' for byte",
		}
	})
	tests.Add("unsupported type", func() interface{} {
		type conf struct {
			Foo complex64
		}
		return tst{
			target: &conf{},
			env: map[string]string{
				"FOO": "'pink'",
			},
			err: "Foo: complex64 conversion not supported",
		}
	})
	tests.Add("unfulfilled requirement", func() interface{} {
		type conf struct {
			Foo int `conf:",required"`
		}
		return tst{
			target: &conf{},
			env:    map[string]string{},
			err:    "Foo: Foo is required",
		}
	})
	tests.Add("default fallthrough", func() interface{} {
		type conf struct {
			Foo int `conf:"FOO"`
		}
		return tst{
			target: &conf{
				Foo: 123,
			},
			env: map[string]string{},
			expected: &conf{
				Foo: 123,
			},
		}
	})
	tests.Add("default required", func() interface{} {
		type conf struct {
			Foo int `conf:"FOO,required"`
		}
		return tst{
			target: &conf{
				Foo: 123,
			},
			env: map[string]string{},
			expected: &conf{
				Foo: 123,
			},
		}
	})
	tests.Add("skip field", func() interface{} {
		type conf struct {
			Foo int `conf:"-"`
			Bar int
		}
		return tst{
			target: &conf{},
			env: map[string]string{
				"FOO": "123",
				"BAR": "234",
			},
			expected: &conf{
				Bar: 234,
			},
		}
	})

	tests.Run(t, func(t *testing.T, test tst) {
		defer testy.RestoreEnv()()
		os.Clearenv()
		if err := testy.SetEnv(test.env); err != nil {
			t.Fatal(err)
		}
		err := test.dec.ReadConfig(test.target)
		testy.Error(t, test.err, err)
		if d := testy.DiffInterface(test.expected, test.target); d != nil {
			t.Error(d)
		}
	})
}

func TestReadEnv(t *testing.T) {
	type tst struct {
		env      map[string]string
		dec      Decoder
		expected map[string]string
	}
	tests := testy.NewTable()
	tests.Add("simple", tst{
		env: map[string]string{
			"FOO": "123",
		},
		expected: map[string]string{
			"FOO": "123",
		},
	})
	tests.Add("prefix", tst{
		env: map[string]string{
			"PRE_FOO": "123",
		},
		dec: Decoder{
			Prefix: "PRE_",
		},
		expected: map[string]string{
			"FOO": "123",
		},
	})

	tests.Run(t, func(t *testing.T, test tst) {
		defer testy.RestoreEnv()()
		os.Clearenv()
		if err := testy.SetEnv(test.env); err != nil {
			t.Fatal(err)
		}
		result := test.dec.readEnv()
		if d := testy.DiffInterface(test.expected, result); d != nil {
			t.Error(d)
		}
	})
}
