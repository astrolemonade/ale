package envconf

import (
	"encoding"
	"errors"
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"
	"unicode/utf8"
)

// ParseError represents a parsing error
type ParseError struct {
	// Field is the name of the target struct field associated with the error.
	Field string
	// Err is the underlying error
	Err error
}

var _ error = &ParseError{}

func (e *ParseError) Error() string {
	return fmt.Sprintf("%s: %s", e.Field, e.Err)
}

// Cause satisfies the github.com/pkg/errors.causer interface
func (e *ParseError) Cause() error {
	return e.Err
}

// Decoder is a configured configuration decoder.
type Decoder struct {
	// Prefix, when set, is stripped from the beginning of environment
	// variables. This value is case-sensitive.
	Prefix string

	// TagName is the struct tag name used to read envconf metadata from
	// struct fields. Defaults to 'conf'.
	TagName string
}

// ReadConfig parses the enironment according to the Decoder configuration, and
// stores the result in dst.
//
// To use default values, set them in dst before calling ReadConfig().
func (d *Decoder) ReadConfig(dst interface{}) error {
	typ := reflect.TypeOf(dst)
	if typ.Kind() != reflect.Ptr {
		return errors.New("dst must be a pointer")
	}
	typ = typ.Elem()
	val := reflect.ValueOf(dst).Elem()
	if typ.Kind() == reflect.Ptr {
		return d.ReadConfig(val.Interface())
	}
	if typ.Kind() != reflect.Struct {
		return errors.New("dst must be a pointer to struct")
	}
	env := d.readEnv()
	ENV := make(map[string]string, len(env))
	for k, v := range env {
		ENV[strings.ToUpper(k)] = v
	}
	for i := 0; i < typ.NumField(); i++ {
		fieldName := typ.Field(i).Name
		fieldVal := val.Field(i)
		tag, required := d.parseTag(typ.Field(i).Tag)
		if tag == "-" {
			continue
		}
		if tag != "" {
			if v, ok := env[tag]; ok {
				if err := set(fieldVal, v); err != nil {
					return &ParseError{Field: fieldName, Err: err}
				}
				continue
			}
		} else {
			if v, ok := env[fieldName]; ok {
				if err := set(fieldVal, v); err != nil {
					return &ParseError{Field: fieldName, Err: err}
				}
				continue
			}
			if v, ok := ENV[strings.ToUpper(fieldName)]; ok {
				if err := set(fieldVal, v); err != nil {
					return &ParseError{Field: fieldName, Err: err}
				}
				continue
			}
		}
		if required && fieldVal.Interface() == reflect.Zero(fieldVal.Type()).Interface() {
			if tag == "" {
				tag = fieldName
			}
			return &ParseError{
				Field: fieldName,
				Err:   fmt.Errorf("%s is required", tag),
			}
		}
	}
	return nil
}

func (d *Decoder) parseTag(t reflect.StructTag) (field string, required bool) {
	tagName := "conf"
	if d.TagName != "" {
		tagName = d.TagName
	}
	parts := strings.Split(t.Get(tagName), ",")
	return parts[0],
		len(parts) > 1 && parts[1] == "required"
}

func set(tgt reflect.Value, val string) error {
	if tgt.Kind() == reflect.Ptr && tgt.IsNil() {
		newVal := reflect.New(tgt.Type().Elem())
		tgt.Set(newVal)
	}
	if unmarshaler, ok := tgt.Interface().(encoding.TextUnmarshaler); ok {
		return unmarshaler.UnmarshalText([]byte(val))
	}
	if unmarshaler, ok := tgt.Addr().Interface().(encoding.TextUnmarshaler); ok {
		return unmarshaler.UnmarshalText([]byte(val))
	}
	if tgt.Kind() == reflect.Ptr {
		tgt = tgt.Elem()
	}
	var v interface{}
	var err error
	switch tgt.Interface().(type) {
	case bool:
		switch strings.ToLower(val) {
		case "true", "t", "1":
			v = true
		case "false", "f", "0", "":
			v = false
		default:
			err = fmt.Errorf("Invalid value '%s' for boolean", val)
		}
	case string:
		v = val
	case int:
		v, err = strconv.ParseInt(val, 10, strconv.IntSize)
		v = int(v.(int64))
	case int8:
		v, err = strconv.ParseInt(val, 10, 8)
		v = int8(v.(int64))
	case int16:
		v, err = strconv.ParseInt(val, 10, 16)
		v = int16(v.(int64))
	case int32: // also rune
		if st := strings.Trim(val, "'"); st != val {
			if utf8.RuneCountInString(st) > 1 {
				return fmt.Errorf("Invalid value '%s' for rune", st)
			}
			v, _ = utf8.DecodeRuneInString(st)
		} else {
			v, err = strconv.ParseInt(val, 10, 32)
			v = int32(v.(int64))
		}
	case int64:
		v, err = strconv.ParseInt(val, 10, 64)
	case uint:
		v, err = strconv.ParseUint(val, 10, strconv.IntSize)
		v = uint(v.(uint64))
	case uint8: // also byte
		if st := strings.Trim(val, "'"); st != val {
			if len(st) > 1 {
				return fmt.Errorf("Invalid value '%s' for byte", st)
			}
			v = st[0]
		} else {
			v, err = strconv.ParseUint(val, 10, 8)
			v = uint8(v.(uint64))
		}
	case uint16:
		v, err = strconv.ParseUint(val, 10, 16)
		v = uint16(v.(uint64))
	case uint32:
		v, err = strconv.ParseUint(val, 10, 32)
		v = uint32(v.(uint64))
	case uint64:
		v, err = strconv.ParseUint(val, 10, 64)
	case float32:
		v, err = strconv.ParseFloat(val, 32)
		v = float32(v.(float64))
	case float64:
		v, err = strconv.ParseFloat(val, 64)
	default:
		return fmt.Errorf("%T conversion not supported", tgt.Interface())
	}
	if err != nil {
		return err
	}
	tgt.Set(reflect.ValueOf(v))
	return nil
}

func (d *Decoder) readEnv() map[string]string {
	env := os.Environ()
	result := make(map[string]string, len(env))
	for _, kv := range env {
		parts := strings.SplitN(kv, "=", 2)
		result[strings.TrimPrefix(parts[0], d.Prefix)] = parts[1]
	}
	return result
}
