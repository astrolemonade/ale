// Package envconf provides simple functionality for reading configuration
// from the environment, such as for use within a 12-Factor Application.
//
package envconf
