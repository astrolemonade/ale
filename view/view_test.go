package view

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"gitlab.com/flimzy/testy"

	"gitlab.com/flimzy/ale/httperr"
)

func TestRender(t *testing.T) {
	type tst struct {
		conf     Config
		req      *http.Request
		expected *http.Response
		stderr   string
	}
	tests := testy.NewTable()
	tests.Add("not found", tst{
		conf: Config{
			SearchPath: []string{"testdata/testX"},
		},
		req:    newRequest("GET", "/index.html", nil),
		stderr: "[ErrorHandler] Not Found\n",
		expected: &http.Response{
			StatusCode: http.StatusNotFound,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"Content-Type":           []string{"text/plain; charset=utf-8"},
				"X-Content-Type-Options": []string{"nosniff"},
			},
			Body: ioutil.NopCloser(strings.NewReader("404 Not Found")),
		},
	})
	tests.Add("invalid template", tst{
		conf: Config{
			SearchPath:        []string{"testdata/test1"},
			TemplateExtension: "tmpl",
		},
		req:    newRequest(http.MethodGet, "/invalid.html", nil),
		stderr: `[ErrorHandler] template: invalid.html.tmpl:1: unexpected "}" in operand`,
		expected: &http.Response{
			StatusCode: http.StatusInternalServerError,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"Content-Type":           []string{"text/plain; charset=utf-8"},
				"X-Content-Type-Options": []string{"nosniff"},
			},
			Body: ioutil.NopCloser(strings.NewReader("500 Internal Server Error")),
		},
	})
	tests.Add("single template", func() interface{} {
		req := newRequest(http.MethodGet, "/index.html", nil)
		data := ReqGetData(req)
		data["Title"] = "Test title"
		return tst{
			conf: Config{
				SearchPath:        []string{"testdata/test1"},
				TemplateExtension: "tmpl",
			},
			req: req,
			expected: &http.Response{
				StatusCode: http.StatusOK,
				ProtoMajor: 1,
				ProtoMinor: 1,
				Header: http.Header{
					"Content-Type": []string{"text/html; charset=utf-8"},
				},
				Body: ioutil.NopCloser(strings.NewReader("<title>Test title</title>")),
			},
		}
	})
	tests.Add("helpers", tst{
		conf: Config{
			SearchPath:        []string{"testdata/test2"},
			Include:           []string{"testdata/helper1.tmpl"},
			TemplateExtension: "tmpl",
		},
		req: newRequest(http.MethodGet, "/foo.html", nil),
		expected: &http.Response{
			StatusCode: http.StatusOK,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"Content-Type": []string{"text/plain; charset=utf-8"},
			},
			Body: ioutil.NopCloser(strings.NewReader("\nFoo!\n\n")),
		},
	})
	tests.Add("entry point", func() interface{} {
		req := newRequest(http.MethodGet, "/foo.html", nil)
		data := ReqGetData(req)
		data["Title"] = "Entry Point Test"
		return tst{
			conf: Config{
				SearchPath:        []string{"testdata/test5"},
				Include:           []string{"testdata/helper2.tmpl"},
				EntryPoint:        "main",
				TemplateExtension: "tmpl",
			},
			req: req,
			expected: &http.Response{
				StatusCode: http.StatusOK,
				ProtoMajor: 1,
				ProtoMinor: 1,
				Header: http.Header{
					"Content-Type": []string{"text/html; charset=utf-8"},
				},
				Body: ioutil.NopCloser(strings.NewReader(`
<html>
<head>
    <title>Entry Point Test</title>
</head>
<body>

This is some content

</body>
</html>`)),
			},
		}
	})

	tests.Run(t, func(t *testing.T, test tst) {
		tmpl, err := New(test.conf)
		if err != nil {
			t.Fatal(err)
		}
		w := httptest.NewRecorder()
		stdout, stderr := testy.RedirIO(nil, func() {
			tmpl.Render(w, test.req)
		})
		if _, err := io.Copy(os.Stdout, stdout); err != nil {
			t.Fatal(err)
		}
		res := w.Result()
		if d := testy.DiffText(test.stderr, stderr); d != nil {
			t.Errorf("Unexpected STDERR\n%s", d)
		}
		if d := testy.DiffHTTPResponse(test.expected, res); d != nil {
			t.Error(d)
		}
	})
}

func TestResolvePath(t *testing.T) {
	type tst struct {
		conf     Config
		req      *http.Request
		expected string
		status   int
		err      string
	}
	tests := testy.NewTable()

	tests.Add("not found", tst{
		req:    newRequest(http.MethodGet, "/", nil),
		status: http.StatusNotFound,
		err:    "Not Found",
	})
	tests.Add("from context", func() interface{} {
		req := newRequest(http.MethodGet, "/", nil)
		data := ReqGetData(req)
		data["_template"] = "/foo/index.html"
		return tst{
			conf: Config{
				SearchPath: []string{"testdata/test4"},
			},
			req:      req,
			expected: "testdata/test4/foo/index.html",
		}
	})
	tests.Add("Wrong root path", tst{
		conf: Config{
			RootPath: "/foo/bar",
		},
		req:    newRequest(http.MethodGet, "/", nil),
		status: http.StatusInternalServerError,
		err:    "Requested path '/' not under RootPath '/foo/bar'",
	})
	tests.Add("from search path", func() interface{} {
		return tst{
			conf: Config{
				SearchPath:        []string{"testdata/test1"},
				TemplateExtension: "tmpl",
			},
			req:      newRequest(http.MethodGet, "/index.html", nil),
			expected: "testdata/test1/index.html.tmpl",
		}
	})
	tests.Add("missing perms", func(t *testing.T) interface{} {
		dir, err := ioutil.TempDir("", "ale-view")
		if err != nil {
			t.Fatal(err)
		}
		tests.Cleanup(func() {
			_ = os.RemoveAll(dir)
		})
		if e := os.Chmod(dir, 0666); e != nil {
			t.Fatal(e)
		}
		stat, err := os.Stat(dir)
		if err != nil {
			t.Fatal(err)
		}
		if stat.Mode() != 0666 {
			t.Skip("CHMOD not supported by test environment")
		}
		return tst{
			conf: Config{
				SearchPath:        []string{dir},
				TemplateExtension: "tmpl",
			},
			req:    newRequest(http.MethodGet, "/index.html", nil),
			status: http.StatusInternalServerError,
			err:    fmt.Sprintf("stat %s/index.html.tmpl: permission denied", dir),
		}
	})
	tests.Add("dir instead of tmpl", func() interface{} {
		return tst{
			conf: Config{
				SearchPath:        []string{"testdata/test3"},
				TemplateExtension: "tmpl",
			},
			req:    newRequest(http.MethodGet, "/index.html", nil),
			status: http.StatusInternalServerError,
			err:    "'testdata/test3/index.html.tmpl' is a dir, expected a template",
		}
	})
	tests.Add("no extension", func() interface{} {
		return tst{
			conf: Config{
				SearchPath: []string{"testdata/test4"},
			},
			req:      newRequest(http.MethodGet, "/foo/index.html", nil),
			expected: "testdata/test4/foo/index.html",
		}
	})
	tests.Add("symlink", func() interface{} {
		return tst{
			conf: Config{
				SearchPath: []string{"testdata/test4"},
			},
			req:      newRequest(http.MethodGet, "/bar/index.html", nil),
			expected: "testdata/test4/bar/index.html",
		}
	})
	tests.Add("root dir", func() interface{} {
		return tst{
			conf: Config{
				SearchPath:        []string{"testdata/test1"},
				RootPath:          "/foo/",
				TemplateExtension: ".tmpl",
			},
			req:      newRequest(http.MethodGet, "/foo/index.html", nil),
			expected: "testdata/test1/index.html.tmpl",
		}
	})
	tests.Add("bare root dir", tst{
		conf: Config{
			SearchPath:        []string{"testdata/test1"},
			RootPath:          "foo",
			TemplateExtension: ".tmpl",
		},
		req:      newRequest(http.MethodGet, "/foo/index.html", nil),
		expected: "testdata/test1/index.html.tmpl",
	})
	tests.Add("directory index", tst{
		conf: Config{
			SearchPath:        []string{"testdata/test1"},
			TemplateExtension: "tmpl",
			DirectoryIndex:    []string{"index.html"},
		},
		req:      newRequest(http.MethodGet, "/", nil),
		expected: "testdata/test1/index.html.tmpl",
	})

	tests.Run(t, func(t *testing.T, test tst) {
		tmpl, err := New(test.conf)
		if err != nil {
			t.Fatal(err)
		}
		result, err := tmpl.resolvePath(test.req)
		testy.StatusError(t, test.err, test.status, err)
		if result != test.expected {
			t.Errorf("Expected: %s\n  Actual: %s\n", test.expected, result)
		}
	})
}

func TestRenderError(t *testing.T) {
	type tst struct {
		conf     Config
		req      *http.Request
		stderr   string
		err      error
		expected *http.Response
	}
	tests := testy.NewTable()
	tests.Add("default not found", tst{
		err:    httperr.New(http.StatusNotFound, "oink"),
		stderr: "[ErrorHandler] oink\n",
		expected: &http.Response{
			StatusCode: http.StatusNotFound,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"Content-Type":           []string{"text/plain; charset=utf-8"},
				"X-Content-Type-Options": []string{"nosniff"},
			},
			Body: ioutil.NopCloser(strings.NewReader("404 Not Found")),
		},
	})
	tests.Add("default error", tst{
		err:    httperr.New(http.StatusBadRequest, "oink"),
		stderr: "[ErrorHandler] oink\n",
		expected: &http.Response{
			StatusCode: http.StatusBadRequest,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"Content-Type":           []string{"text/plain; charset=utf-8"},
				"X-Content-Type-Options": []string{"nosniff"},
			},
			Body: ioutil.NopCloser(strings.NewReader("400 Bad Request")),
		},
	})

	tests.Run(t, func(t *testing.T, test tst) {
		tmpl, err := New(test.conf)
		if err != nil {
			t.Fatal(err)
		}
		w := httptest.NewRecorder()
		req := test.req
		if req == nil {
			req = newRequest(http.MethodGet, "/", nil)
		}
		_, stderr := testy.RedirIO(nil, func() {
			tmpl.RenderError(w, req, test.err)
		})
		res := w.Result()
		if d := testy.DiffText(test.stderr, stderr); d != nil {
			t.Errorf("Unexpected STDERR\n%s", d)
		}
		if d := testy.DiffHTTPResponse(test.expected, res); d != nil {
			t.Error(d)
		}
	})
}

func TestMiddleware(t *testing.T) {
	type tst struct {
		conf     Config
		next     http.Handler
		req      *http.Request
		stderr   string
		expected *http.Response
	}
	tests := testy.NewTable()
	tests.Add("next writes", tst{
		next: http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
			_, _ = w.Write([]byte("Yay!"))
		}),
		req: newRequest(http.MethodPut, "/", strings.NewReader("")),
		expected: &http.Response{
			StatusCode: http.StatusOK,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"Content-Type": []string{"text/plain; charset=utf-8"},
			},
			Body: ioutil.NopCloser(strings.NewReader("Yay!")),
		},
	})
	tests.Add("view renders", tst{
		conf: Config{
			SearchPath:        []string{"testdata/test1"},
			TemplateExtension: "tmpl",
		},
		next: http.HandlerFunc(func(_ http.ResponseWriter, r *http.Request) {
			data := ReqGetData(r)
			data["Title"] = "Oink"
		}),
		req: newRequest(http.MethodGet, "/index.html", nil),
		expected: &http.Response{
			StatusCode: http.StatusOK,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"Content-Type": []string{"text/html; charset=utf-8"},
			},
			Body: ioutil.NopCloser(strings.NewReader("<title>Oink</title>")),
		},
	})
	tests.Add("render error", tst{
		conf: Config{
			SearchPath:        []string{"testdata/test1"},
			TemplateExtension: "foo",
		},
		next: http.HandlerFunc(func(_ http.ResponseWriter, _ *http.Request) {}),
		req:  newRequest(http.MethodGet, "/index.html", nil),
		expected: &http.Response{
			StatusCode: http.StatusNotFound,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"Content-Type":           []string{"text/plain; charset=utf-8"},
				"X-Content-Type-Options": []string{"nosniff"},
			},
			Body: ioutil.NopCloser(strings.NewReader("404 Not Found")),
		},
		stderr: "[ErrorHandler] Not Found",
	})

	tests.Run(t, func(t *testing.T, test tst) {
		tmpl, err := New(test.conf)
		if err != nil {
			t.Fatal(err)
		}
		w := httptest.NewRecorder()
		h := tmpl.Middleware(test.next)
		_, stderr := testy.RedirIO(nil, func() {
			h.ServeHTTP(w, test.req)
		})
		res := w.Result()
		if d := testy.DiffText(test.stderr, stderr); d != nil {
			t.Errorf("Unexpected STDERR output:\n%s", d)
		}
		if d := testy.DiffHTTPResponse(test.expected, res); d != nil {
			t.Error(d)
		}
	})
}

func TestHandle(t *testing.T) {
	type tst struct {
		conf     Config
		req      *http.Request
		handler  ErrorHandlerFunc
		expected *http.Response
		stderr   string
	}
	tests := testy.NewTable()
	tests.Add("handler writes", tst{
		handler: func(w http.ResponseWriter, _ *http.Request) error {
			_, _ = w.Write([]byte("Yay!"))
			return nil
		},
		req: newRequest(http.MethodGet, "/", nil),
		expected: &http.Response{
			StatusCode: http.StatusOK,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"Content-Type": []string{"text/plain; charset=utf-8"},
			},
			Body: ioutil.NopCloser(strings.NewReader("Yay!")),
		},
	})
	tests.Add("view renders", tst{
		conf: Config{
			SearchPath:        []string{"testdata/test1"},
			TemplateExtension: "tmpl",
		},
		handler: func(_ http.ResponseWriter, r *http.Request) error {
			data := ReqGetData(r)
			data["Title"] = "Oink"
			return nil
		},
		req: newRequest(http.MethodGet, "/index.html", nil),
		expected: &http.Response{
			StatusCode: http.StatusOK,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"Content-Type": []string{"text/html; charset=utf-8"},
			},
			Body: ioutil.NopCloser(strings.NewReader("<title>Oink</title>")),
		},
	})
	tests.Add("render error", tst{
		conf: Config{
			SearchPath:        []string{"testdata/test1"},
			TemplateExtension: "foo",
		},
		handler: func(_ http.ResponseWriter, _ *http.Request) error {
			return nil
		},
		req: newRequest(http.MethodGet, "/index.html", nil),
		expected: &http.Response{
			StatusCode: http.StatusNotFound,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"Content-Type":           []string{"text/plain; charset=utf-8"},
				"X-Content-Type-Options": []string{"nosniff"},
			},
			Body: ioutil.NopCloser(strings.NewReader("404 Not Found")),
		},
		stderr: "[ErrorHandler] Not Found",
	})
	tests.Add("handler error", tst{
		conf: Config{
			SearchPath:        []string{"testdata/test1"},
			TemplateExtension: "foo",
		},
		handler: func(_ http.ResponseWriter, _ *http.Request) error {
			return errors.New("Foo error")
		},
		req: newRequest(http.MethodGet, "/index.html", nil),
		expected: &http.Response{
			StatusCode: http.StatusInternalServerError,
			ProtoMajor: 1,
			ProtoMinor: 1,
			Header: http.Header{
				"Content-Type":           []string{"text/plain; charset=utf-8"},
				"X-Content-Type-Options": []string{"nosniff"},
			},
			Body: ioutil.NopCloser(strings.NewReader("500 Internal Server Error")),
		},
		stderr: "[ErrorHandler] Foo error",
	})
	tests.Run(t, func(t *testing.T, test tst) {
		tmpl, err := New(test.conf)
		if err != nil {
			t.Fatal(err)
		}
		w := httptest.NewRecorder()
		h := tmpl.Handle(test.handler)
		_, stderr := testy.RedirIO(nil, func() {
			h.ServeHTTP(w, test.req)
		})
		res := w.Result()
		if d := testy.DiffText(test.stderr, stderr); d != nil {
			t.Errorf("Unexpected STDERR output:\n%s", d)
		}
		if d := testy.DiffHTTPResponse(test.expected, res); d != nil {
			t.Error(d)
		}
	})
}
