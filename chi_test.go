package ale_test

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/flimzy/ale"
	"gitlab.com/flimzy/testy"
)

func server(t *testing.T) http.Handler {
	t.Helper()

	r := ale.NewRouter()
	r.Use(ale.Recover())
	r.Use(func(next ale.Handler) ale.Handler {
		return ale.HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			w.Header().Add("X-Middleware", "one")
			return next.ServeHTTPE(w, r)
		})
	})

	r.Get("/", func(w http.ResponseWriter, r *http.Request) error {
		fmt.Fprintln(w, "This is it!")
		return nil
	})

	g := r.Group()
	g.Use(func(next ale.Handler) ale.Handler {
		return ale.HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			w.Header().Add("X-Middleware", "group")
			return next.ServeHTTPE(w, r)
		})
	})
	g.Get("/group", func(w http.ResponseWriter, r *http.Request) error {
		fmt.Fprintln(w, "this is the group")
		return nil
	})
	g.Handle("/handle", ale.HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
		fmt.Fprint(w, "handle endpoint")
		return nil
	}))
	g.HandleFunc("/handlefunc", func(w http.ResponseWriter, r *http.Request) error {
		fmt.Fprint(w, "handle func endpoint")
		return nil
	})

	r2 := r.Route("/sub")
	r2.Use(func(next ale.Handler) ale.Handler {
		return ale.HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
			w.Header().Add("X-Middleware", "subroute")
			return next.ServeHTTPE(w, r)
		})
	})
	r2.Get("/router", func(w http.ResponseWriter, r *http.Request) error {
		fmt.Fprint(w, "sub router")
		return nil
	})
	r2.Get("/error", func(http.ResponseWriter, *http.Request) error {
		return errors.New("error endpoint")
	})

	return r
}

func Test_router_logging(t *testing.T) {
	r := server(t).(ale.Router)
	var logCtx *ale.LogContext
	r.Logger(func(c *ale.LogContext) {
		c.StartTime = time.Time{}
		c.ElapsedTime = 0
		logCtx = c
	})

	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/", nil)

	r.ServeHTTP(rec, req)

	if d := testy.DiffInterface(testy.Snapshot(t), logCtx); d != nil {
		t.Error(d)
	}
}

func Test_router_reporter(t *testing.T) {
	r := server(t).(ale.Router)
	var err error
	r.ErrorReporter(func(_ http.ResponseWriter, _ *http.Request, e error) {
		err = e
	})

	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/sub/error", nil)

	r.ServeHTTP(rec, req)

	if !testy.ErrorMatches("error endpoint", err) {
		t.Errorf("Unexpected error: %q", err)
	}

}

func Test_router(t *testing.T) {
	type tt struct {
		method, path string
		body         io.Reader
	}

	tests := testy.NewTable()
	tests.Add("not found", tt{
		method: http.MethodGet,
		path:   "/not/found",
	})
	tests.Add("found", tt{
		method: http.MethodGet,
		path:   "/",
	})
	tests.Add("group", tt{
		method: http.MethodGet,
		path:   "/group",
	})
	tests.Add("Handle", tt{
		method: http.MethodGet,
		path:   "/handle",
	})
	tests.Add("HandleFunc", tt{
		method: http.MethodGet,
		path:   "/handlefunc",
	})
	tests.Add("subrouter", tt{
		method: http.MethodGet,
		path:   "/sub/router",
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		s := server(t)

		rec := httptest.NewRecorder()
		req := httptest.NewRequest(tt.method, tt.path, tt.body)

		s.ServeHTTP(rec, req)

		res := rec.Result()
		defer res.Body.Close()

		if d := testy.DiffHTTPResponse(testy.Snapshot(t), res); d != nil {
			t.Error(d)
		}

	})
}
