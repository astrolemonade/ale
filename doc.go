/*

Package ale provides some utilites to extend standard HTTP handlers for
easier error handling.

The core of this package is the ErrorHandler and ErrorHandlerFunc types, which
simply extends their standard library companion types (http.Handler and
http.HandlerFunc respectively) to return errors.

Such a handler (or middleware) should either write a response to the standard
http.ResponseWriter _or_ return an error. Any such errors are converted to a
response by an ErrorReporter.  This allows putting all error-handling logic in
a single location, rather than scattered throughout your application.

*/
package ale
