package ale

import (
	"fmt"
	"net/http"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/flimzy/ale/errors"
)

// Recover converts any panic into an error. Requires use of HandleErrors.
func Recover() func(Handler) Handler {
	return func(next Handler) Handler {
		return HandlerFunc(func(w http.ResponseWriter, r *http.Request) (err error) {
			defer func() {
				if r := recover(); r != nil {
					err = panicToError(r, 2)
				}
			}()
			return next.ServeHTTPE(w, r)
		})
	}
}

// PanicToError converts a recovered panic to an error. The returned error
// implements the HTTPStatus() and GRPCStatus() methods, for compatibility.
func PanicToError(p interface{}) error {
	return panicToError(p, 1)
}

func panicToError(p interface{}, skip int) error {
	if p == nil {
		return nil
	}
	var err error
	switch t := p.(type) {
	case panicErr:
		return t.error
	case error:
		err = t
	default:
		err = fmt.Errorf("%v", p)
	}
	if stack := errors.InspectStackTrace(err); stack == nil {
		err = errors.WithStackN(err, skip+1)
	}
	return &recovered{err}
}

type recovered struct {
	error
}

func (r recovered) Error() string {
	return "panic recovered: " + r.error.Error()
}

func (r recovered) GRPCStatus() *status.Status {
	return status.New(codes.Internal, r.Error())
}

func (r recovered) HTTPStatus() int {
	return http.StatusInternalServerError
}

func (r recovered) Unwrap() error {
	return r.error
}
