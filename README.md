![Ale logo](artwork/ale-logo.png)

Ale
---

Ale is a collection of HTTP-related tools to be used with roll-your-own Go
web applications.

This software is released under the MIT license, as outlined in the included
LICENSE.md file.
