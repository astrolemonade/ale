package ale

import (
	"net/http"
	"strings"
	"sync/atomic"
	"time"
)

// LogContext captures data about a request and response, for logging.
type LogContext struct {
	w http.ResponseWriter
	// Request is the client request.
	Request *http.Request

	// StatusCode is the HTTP status code sent to the client.
	StatusCode int
	// ResponseHeader is the list of header values sent to the client.
	ResponseHeader http.Header
	// ResponseHeader is the list of trailer values sent to the client.
	ResponseTrailer http.Header
	// ResponseBytes is the number of bytes written in the response body.
	ResponseBytes int64
	// RequestBytes is the number of bytes read from the request body. This
	// can differ from the Content-Length value if reading is not completed, or
	// in case Content-Length is unset.
	RequestBytes int64

	// StartTime is the time the request was received by the server
	StartTime time.Time
	// ElapsedTime is the duration it took to serve the request
	ElapsedTime time.Duration

	// Error is the error, if any, returned from the handler/middlware chain.
	Error error

	// Data is a collection of arbitrary request-scoped data to be logged.
	// Add to this by calling LogData.
	Data map[string]interface{}

	// written is an atomic boolean to indicate whether the http.ResponseWriter
	// has been written to.
	written int32
}

var _ http.ResponseWriter = &LogContext{}

// Used returns true if the this http.ResponseWriter has been written to.
func (l *LogContext) Used() bool {
	return atomic.LoadInt32(&l.written) != 0
}

// Header returns the header map that will be sent by
// WriteHeader.
func (l *LogContext) Header() http.Header {
	return l.w.Header()
}

// WriteHeader sends an HTTP response header with the provided
// status code.
func (l *LogContext) WriteHeader(status int) {
	l.w.WriteHeader(status)
	if atomic.SwapInt32(&l.written, 1) != 1 {
		l.StatusCode = status
		l.setResponseHeader()
	}
}

// Write writes the data to the connection as part of an HTTP reply.
func (l *LogContext) Write(p []byte) (int, error) {
	atomic.StoreInt32(&l.written, 1)
	n, err := l.w.Write(p)
	atomic.AddInt64(&l.ResponseBytes, int64(n))
	return n, err
}

func (l *LogContext) setResponseHeader() {
	head := l.w.Header()
	l.ResponseHeader = http.Header{}
	trailer := make(map[string]struct{})
	for _, k := range head["Trailer"] {
		trailer[k] = struct{}{}
	}
	for k, v := range head {
		if k == "Trailer" || strings.HasPrefix(k, http.TrailerPrefix) {
			continue
		}
		if _, ok := trailer[k]; ok {
			continue
		}
		l.ResponseHeader[k] = v
	}
}

func (l *LogContext) setResponseTrailer() {
	head := l.w.Header()
	l.ResponseTrailer = http.Header{}
	trailer := make(map[string]struct{})
	for _, k := range head["Trailer"] {
		trailer[k] = struct{}{}
	}
	for k, v := range head {
		if strings.HasPrefix(k, http.TrailerPrefix) {
			l.ResponseTrailer[strings.TrimPrefix(k, http.TrailerPrefix)] = v
		}
		if _, ok := trailer[k]; ok {
			l.ResponseTrailer[k] = v
		}
	}
}

// NewLogContext returns a new log context, which should be used as a
// ResponseWriter for subsequent handlers in middleware.
//
// Example:
//
//    func LogMiddleware(next http.Handler) http.Handler {
//        return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
//            logCtx := NewLogContext(w,r)
//            next.ServeHTTP(logCtx, r)
//            logCtx.Finalize()
//            // Log logCtx here
//        })
//    }
func NewLogContext(w http.ResponseWriter, r *http.Request) *LogContext {
	return &LogContext{
		w:         w,
		Request:   r,
		StartTime: now(),
		Data:      map[string]interface{}{},
	}
}

var now = time.Now

// Finalize should be called after ServeHTTP, to finalize the response values.
// Without this call, trailers will not be set, and elapsed time is not
// calculated.
func (l *LogContext) Finalize() {
	if !l.Used() {
		l.WriteHeader(http.StatusOK)
	}
	l.setResponseTrailer()
	l.ElapsedTime = now().Sub(l.StartTime)
}
