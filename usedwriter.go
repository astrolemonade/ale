package ale

import (
	"errors"
	"net/http"
)

// UsedWriter extends the http.ResponseWriter interface to return a bool to
// indicate whether anything has been written.
//
// To use it, call the TrackWriterUsage middleware early in your middleware
// stack. Then in other middlewares or handlers, you can use the WasUsed
// method to check the status.
//
//  func main() {
//      r := chi.NewRouter()
//
//      r.Use(ale.TrackWriterUsage)
//      // and other middlewares
//
//      r.Get("/", func(w http.ResponseWriter, r *http.Request) {
//          if done, _ := ale.WasUsed(w); done {
//              // Nothing to do, a response was already sent
//              return
//          }
//
//          // Normal operation here...
//
//      })
//  }
type UsedWriter interface {
	http.ResponseWriter
	Used() bool
}

type usedWriter struct {
	http.ResponseWriter
	used bool
}

// WriteHeader wraps the underlying WriteHeader method.
func (w *usedWriter) WriteHeader(status int) {
	w.used = true
	w.ResponseWriter.WriteHeader(status)
}

func (w *usedWriter) Write(b []byte) (int, error) {
	w.used = true
	return w.ResponseWriter.Write(b)
}

func (w *usedWriter) Used() bool {
	return w.used
}

// WriterIsDIsUsedone returns true if a response has been written. An error is
// returned if the underlying writer is not a DoneWriter.
func IsUsed(w http.ResponseWriter) (bool, error) {
	if dw, ok := w.(UsedWriter); ok {
		return dw.Used(), nil
	}
	return false, errors.New("not a UsedWriter")
}

// TrackWriterUsage is an ale middleware which wraps the standard http.ResponseWriter
// with a DoneWriter.  Subsequent middlewares or handlers should use the
// WriterIsDone method to check the status.
func TrackWriterUsage(next Handler) Handler {
	return HandlerFunc(func(w http.ResponseWriter, r *http.Request) error {
		dw, ok := w.(UsedWriter)
		if !ok {
			dw = &usedWriter{ResponseWriter: w}
		}
		return next.ServeHTTPE(dw, r)
	})
}

// StdTrackWriterUsage is an http middleware equivalent of WrapWriter.
func StdTrackWriterUsage(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		dw, ok := w.(UsedWriter)
		if !ok {
			dw = &usedWriter{ResponseWriter: w}
		}
		next.ServeHTTP(dw, r)
	})
}
