module gitlab.com/flimzy/ale

go 1.15

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.7.2
	github.com/oxtoacart/bpool v0.0.0-20190530202638-03653db5a59c
	github.com/pkg/errors v0.9.1
	gitlab.com/flimzy/testy v0.12.0
	google.golang.org/grpc v1.48.0
)
