//go:build go1.12
// +build go1.12

package panicerr

// Go 1.12 has fewer stack frames in panic stack traces, than older versions.
const skipFrames = 4
