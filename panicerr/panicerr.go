// Package panicerr makes it easy to recover panics, and convert them to
// standard errors.
package panicerr

import (
	"fmt"
)

// Recover wraps the recover() built in, and is meant to be called in a defer
// statement. If a panic is recovered, it is converted to an error, with
// pkg/errors-compatible stack trace, and assigned to err.
//
// Example:
//
//    func DoSomething() (err error) {}
//        defer panicerr.Recover(&err)
//        // ... code that may panic
//        return nil
//     }
func Recover(err *error) {
	r := recover()
	if r == nil {
		return
	}
	switch t := r.(type) {
	case error:
		*err = &withStack{
			error: t,
			stack: callers(),
		}
	case string:
		*err = &fundamental{
			msg:   t,
			stack: callers(),
		}
	default:
		*err = &fundamental{
			msg:   fmt.Sprintf("%v", t),
			stack: callers(),
		}
	}
}
