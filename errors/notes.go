package errors

import (
	"fmt"
	"net/http"
	"time"
)

// Notes is a collection of zero or more error annotations, which can be
// attached to an error.
type Notes interface {
	// HTTPStatus returns a new Notes instance which wraps the original and
	// adds the provided HTTP status.
	HTTPStatus(int) Notes
	// Field returns a new Notes instance which wraps the original and adds the
	// provided key/value field pair.
	Field(string, interface{}) Notes
	// Fields returns a new Notes instance which wraps the original and adds
	// the provided fields.
	Fields(Fields) Notes
	// Wrap annotates the provided error with the annotations. If error is nil
	// nil is returned. A stacktrace is also added where Wrap is called (unless
	// NoStack() has been called).
	Wrap(error) error
	// Errorf returns a new error annotated with the annotations. Equivalent to
	// Wrap(fmt.Errorf(format, args...)).
	Errorf(format string, args ...interface{}) error
	// NoStack returns a new Notes instance which will not include a stack
	// trace when Wrap() is called.
	NoStack() Notes
	// RetryAfter returns a new Notes instance which wraps the original and
	// adds the provided RetryAfter value based on t.
	RetryAfter(time.Time) Notes
	// RetryAfterDur returns a new Notes instance which wraps the original and
	// adds the provided RetryAfter value based on dur.
	RetryAfterDur(time.Duration) Notes
	// NoRetryAfter returns a new Notes instance which wraps the original and
	// adds a blank RetryAfter value.
	NoRetryAfter() Notes
	// StackN instructs the Wrap function to attach a stack trace with s frames
	// skipped.
	StackN(s uint) Notes
}

type notes struct {
	notes      *notes
	fields     Fields
	status     *int
	noStack    bool
	retry      *string
	skipFrames *uint
}

// NewNotes returns an empty Notes collection.
func NewNotes() Notes {
	return &notes{}
}

func (n *notes) HTTPStatus(status int) Notes {
	return &notes{
		notes:  n,
		status: &status,
	}
}

func (n *notes) NoStack() Notes {
	return &notes{
		notes:   n,
		noStack: true,
	}
}

func (n *notes) StackN(s uint) Notes {
	return &notes{
		notes:      n,
		skipFrames: &s,
	}
}

func (n *notes) Field(key string, value interface{}) Notes {
	return &notes{
		notes:  n,
		fields: Fields{key: value},
	}
}

func (n *notes) Fields(fields Fields) Notes {
	return &notes{
		notes:  n,
		fields: fields,
	}
}

func (n *notes) RetryAfter(t time.Time) Notes {
	retry := t.UTC().Format(http.TimeFormat)
	return &notes{
		notes: n,
		retry: &retry,
	}
}

func (n *notes) RetryAfterDur(d time.Duration) Notes {
	retry := fmt.Sprintf("%.0f", d.Seconds())
	return &notes{
		notes: n,
		retry: &retry,
	}
}

func (n *notes) NoRetryAfter() Notes {
	retry := ""
	return &notes{
		notes: n,
		retry: &retry,
	}
}

func (n *notes) Errorf(format string, args ...interface{}) error {
	return n.wrap(fmt.Errorf(format, args...))
}

func (n *notes) Wrap(err error) error {
	return n.wrap(err)
}

func (n *notes) wrap(err error) error {
	if err == nil {
		return nil
	}
	fields := Fields{}
	var status *int
	var noStack bool
	var retry *string
	var skipFrames *uint
	for nn := n; nn != nil; nn = nn.notes {
		if status == nil {
			status = nn.status
		}
		if nn.noStack {
			noStack = true
		}
		if retry == nil {
			retry = nn.retry
		}
		if skipFrames == nil {
			skipFrames = nn.skipFrames
		}
		for k, v := range nn.fields {
			if _, ok := fields[k]; !ok {
				fields[k] = v
			}
		}
	}
	e := err
	if !noStack {
		skip := 1 // Skip one frame; our direct caller
		if skipFrames != nil {
			skip += int(*skipFrames)
		}
		e = &withStack{
			e,
			callers(skip),
		}
	}

	if status != nil {
		e = &withHTTPStatus{
			e,
			*status,
		}
	}
	if retry != nil {
		e = &withRetryAfter{
			e,
			*retry,
		}
	}
	if len(fields) > 0 {
		e = &withFields{
			e,
			fields,
		}
	}

	return e
}
