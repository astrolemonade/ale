package errors

// Fields represents arbitrary key/value fields with which errors may be
// annotated. This is intended for use with structured logging systems such
// as logrus.
type Fields map[string]interface{}

type withFields struct {
	error
	fields Fields
}

func (e *withFields) Error() string {
	return e.error.Error()
}

func (e *withFields) Fields() map[string]interface{} {
	return e.fields
}

func (e *withFields) Unwrap() error {
	return e.error
}

// WithField annotates err with the provided key/value pair, which can be
// inspected with the InspectFields() function. It also adds the stacktrace
// where WithField is called. If err is nil, nil is returned.
func WithField(err error, key string, value interface{}) error {
	if err == nil {
		return nil
	}
	return &withStack{
		&withFields{
			err,
			Fields{key: value},
		},
		callers(0),
	}
}

// WithFields annotates err with the provided fields, which can be inspected
// with the InspectFields() function. It also adds the stacktrace where
// WithFields is called.
func WithFields(err error, fields Fields) error {
	if err == nil {
		return nil
	}
	return &withStack{
		&withFields{
			err,
			fields,
		},
		callers(0),
	}
}

// InspectFields traverses the entire error chain, looking for embedded
// fields. In case of duplicate keys, the outter-most value takes precident.
// If no field annotations are found, nil is returned.
func InspectFields(err error) map[string]interface{} {
	if err == nil {
		return nil
	}
	var ferr interface {
		error
		Fields() map[string]interface{}
	}
	fields := map[string]interface{}{}
	for As(err, &ferr) {
		for k, v := range ferr.Fields() {
			if _, ok := fields[k]; !ok {
				fields[k] = v
			}
		}
		err = Unwrap(ferr)
	}
	if len(fields) > 0 {
		return fields
	}
	return nil
}
