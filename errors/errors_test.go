package errors

import (
	"fmt"
	"regexp"
	"testing"
)

func TestAttachStack(t *testing.T) {
	err := New("foo")    // We want this stack
	err = WithStack(err) // Which is normally obscured by this one

	st := InspectDeepStackTrace(err)
	err = AttachStack(err, st)
	final := InspectStackTrace(err)
	want := regexp.MustCompile(`^\[\]errors\.Frame{errors_test.go:10,`)
	if got := fmt.Sprintf("%#v", final); !want.MatchString(got) {
		t.Errorf("Unexpected output: %s", got)
	}
}
