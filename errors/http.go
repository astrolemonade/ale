package errors

import (
	"fmt"
	"net/http"

	echo3 "github.com/labstack/echo"
	echo4 "github.com/labstack/echo/v4"
)

// twirp: https://pkg.go.dev/github.com/twitchtv/twirp#Error
// grpc: https://pkg.go.dev/google.golang.org/grpc#Code

type httpStatusError interface {
	HTTPStatus() int
}

// ExtractHTTPStatus returns the outter-most HTTP status in the error stack.
// The bool value will be true if err includes an explicit HTTP status code.
//
// If err is nil, 0 is returned.
//
// If no HTTP status is found, http.StatusInternalServerError is returned.
//
// This function understand HTTPStatusErrors (i.e. interface { HTTPStatus() int }),
// as well as echo3 and echo4 HTTPError type.
func ExtractHTTPStatus(err error) (int, bool) {
	if err == nil {
		return 0, false
	}
	var hse httpStatusError
	if As(err, &hse) {
		return hse.HTTPStatus(), true
	}
	e4 := new(echo4.HTTPError)
	if As(err, &e4) {
		return e4.Code, true
	}
	e3 := new(echo3.HTTPError)
	if As(err, &e3) {
		return e3.Code, true
	}
	return http.StatusInternalServerError, false

}

// InspectHTTPStatus returns the outter-most HTTP status in the error stack.
//
// If err is nil, 0 is returned.
//
// If no HTTP status is found, http.StatusInternalServerError is returned.
//
// This function understand HTTPStatusErrors (i.e. interface { HTTPStatus() int }),
// as well as echo3 and echo4 HTTPError type.
//
// TODO: Support gRPC and Twirp errors.
func InspectHTTPStatus(err error) int {
	status, _ := ExtractHTTPStatus(err)
	return status
}

type withHTTPStatus struct {
	error
	status int
}

func (e *withHTTPStatus) Unwrap() error {
	return e.error
}

func (e *withHTTPStatus) Error() string {
	return e.error.Error()
}

func (e *withHTTPStatus) HTTPStatus() int {
	return e.status
}

// Internal promotes err to an Internal Server Error.  Shorter equivalent of
// WithHTTPStatus(err, http.StatusInternalServerError)
func Internal(err error) error {
	return &withStack{
		&withHTTPStatus{
			err,
			http.StatusInternalServerError,
		},
		callers(0),
	}
}

type mustLog struct {
	error
}

// MustLog flags that an error should be logged, regardless of its HTTP status.
func MustLog(err error) error {
	return mustLog{err}
}

func (mustLog) MustLog() {}

// WithHTTPStatus annotates err with the provided HTTP status. If err is nil,
// WithHTTPStatus returns nil.
func WithHTTPStatus(err error, status int) error {
	if err == nil {
		return nil
	}
	return &withStack{
		&withHTTPStatus{
			err,
			status,
		},
		callers(0),
	}
}

// HTTPStatusf works like calling WithHTTPStatus(status, Errorf(format, args ...)).
func HTTPStatusf(status int, format string, args ...interface{}) error {
	return &withStack{
		&withHTTPStatus{
			fmt.Errorf(format, args...),
			status,
		},
		callers(0),
	}
}

// Standard converts a standard HTTP status code into a useable error
type Standard int

var _ httpStatusError = Standard(0)

func (e Standard) Error() string {
	if str := http.StatusText(int(e)); str != "" {
		return str
	}
	return fmt.Sprintf("HTTP status %d", int(e))
}

// HTTPStatus returns the HTTP status associated with the error.
func (e Standard) HTTPStatus() int {
	return int(e)
}

// Standard HTTP status codes as errors.
const (
	StatusBadRequest                   = Standard(http.StatusBadRequest)
	StatusUnauthorized                 = Standard(http.StatusUnauthorized)
	StatusPaymentRequired              = Standard(http.StatusPaymentRequired)
	StatusForbidden                    = Standard(http.StatusForbidden)
	StatusNotFound                     = Standard(http.StatusNotFound)
	StatusMethodNotAllowed             = Standard(http.StatusMethodNotAllowed)
	StatusNotAcceptable                = Standard(http.StatusNotAcceptable)
	StatusProxyAuthRequired            = Standard(http.StatusProxyAuthRequired)
	StatusRequestTimeout               = Standard(http.StatusRequestTimeout)
	StatusConflict                     = Standard(http.StatusConflict)
	StatusGone                         = Standard(http.StatusGone)
	StatusLengthRequired               = Standard(http.StatusLengthRequired)
	StatusPreconditionFailed           = Standard(http.StatusPreconditionFailed)
	StatusRequestEntityTooLarge        = Standard(http.StatusRequestEntityTooLarge)
	StatusRequestURITooLong            = Standard(http.StatusRequestURITooLong)
	StatusUnsupportedMediaType         = Standard(http.StatusUnsupportedMediaType)
	StatusRequestedRangeNotSatisfiable = Standard(http.StatusRequestedRangeNotSatisfiable)
	StatusExpectationFailed            = Standard(http.StatusExpectationFailed)
	StatusTeapot                       = Standard(http.StatusTeapot)
	StatusMisdirectedRequest           = Standard(http.StatusMisdirectedRequest)
	StatusUnprocessableEntity          = Standard(http.StatusUnprocessableEntity)
	StatusLocked                       = Standard(http.StatusLocked)
	StatusFailedDependency             = Standard(http.StatusFailedDependency)
	StatusTooEarly                     = Standard(http.StatusTooEarly)
	StatusUpgradeRequired              = Standard(http.StatusUpgradeRequired)
	StatusPreconditionRequired         = Standard(http.StatusPreconditionRequired)
	StatusTooManyRequests              = Standard(http.StatusTooManyRequests)
	StatusRequestHeaderFieldsTooLarge  = Standard(http.StatusRequestHeaderFieldsTooLarge)
	StatusUnavailableForLegalReasons   = Standard(http.StatusUnavailableForLegalReasons)

	StatusInternalServerError           = Standard(http.StatusInternalServerError)
	StatusNotImplemented                = Standard(http.StatusNotImplemented)
	StatusBadGateway                    = Standard(http.StatusBadGateway)
	StatusServiceUnavailable            = Standard(http.StatusServiceUnavailable)
	StatusGatewayTimeout                = Standard(http.StatusGatewayTimeout)
	StatusHTTPVersionNotSupported       = Standard(http.StatusHTTPVersionNotSupported)
	StatusVariantAlsoNegotiates         = Standard(http.StatusVariantAlsoNegotiates)
	StatusInsufficientStorage           = Standard(http.StatusInsufficientStorage)
	StatusLoopDetected                  = Standard(http.StatusLoopDetected)
	StatusNotExtended                   = Standard(http.StatusNotExtended)
	StatusNetworkAuthenticationRequired = Standard(http.StatusNetworkAuthenticationRequired)
)
