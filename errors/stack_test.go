package errors

import (
	stderr "errors"
	"fmt"
	"net/http"
	"regexp"
	"testing"
)

func TestStackTrace(t *testing.T) {
	tests := []struct {
		err  error
		want []string
	}{
		{
			New("ooh"), []string{
				"gitlab.com/flimzy/ale/errors.TestStackTrace\n" +
					"\t.+/ale/errors/stack_test.go:17",
			},
		},
		{
			Unwrap(Errorf("ahh: %w", New("ooh"))), []string{
				"gitlab.com/flimzy/ale/errors.TestStackTrace\n" +
					"\t.+/ale/errors/stack_test.go:23", // this is the stack of New
			},
		},
		{
			func() error { return New("ooh") }(), []string{
				`gitlab.com/flimzy/ale/errors.TestStackTrace.func1` +
					"\n\t.+/ale/errors/stack_test.go:29", // this is the stack of New
				"gitlab.com/flimzy/ale/errors.TestStackTrace\n" +
					"\t.+/ale/errors/stack_test.go:29", // this is the stack of New's caller
			},
		},
		{
			func() error {
				return func() error {
					return Errorf("hello %s", fmt.Sprintf("world: %s", "ooh"))
				}()
			}(), []string{
				`gitlab.com/flimzy/ale/errors.TestStackTrace.func2.1` +
					"\n\t.+/ale/errors/stack_test.go:39", // this is the stack of Errorf
				`gitlab.com/flimzy/ale/errors.TestStackTrace.func2` +
					"\n\t.+/ale/errors/stack_test.go:40", // this is the stack of Errorf's caller
				"gitlab.com/flimzy/ale/errors.TestStackTrace\n" +
					"\t.+/ale/errors/stack_test.go:41", // this is the stack of Errorf's caller's caller
			},
		},
		{
			WithStack(stderr.New("foo")),
			[]string{
				"gitlab.com/flimzy/ale/errors.TestStackTrace" +
					"\n\t.+/ale/errors/stack_test.go:51",
			},
		},
		{
			Errorf("foo: %w", stderr.New("foo")),
			[]string{
				"gitlab.com/flimzy/ale/errors.TestStackTrace" +
					"\n\t.+/ale/errors/stack_test.go:58",
			},
		},
		{
			WithHTTPStatus(stderr.New("missing"), http.StatusNotFound),
			[]string{
				"gitlab.com/flimzy/ale/errors.TestStackTrace" +
					"\n\t.+/ale/errors/stack_test.go:65",
			},
		},
		{
			HTTPStatusf(http.StatusNotFound, "foo: %w", stderr.New("missing")),
			[]string{
				"gitlab.com/flimzy/ale/errors.TestStackTrace" +
					"\n\t.+/ale/errors/stack_test.go:72",
			},
		},
	}
	for i, tt := range tests {
		var x interface {
			StackTrace() StackTrace
		}
		if As(tt.err, &x) {
			st := x.StackTrace()
			for j, want := range tt.want {
				testFormatRegexp(t, i, st[j], "%+v", want)
			}
			continue
		}
		t.Errorf("%T: expected %#v to implement StackTrace() StackTrace", tt.err, tt.err)
	}
}

func TestInspectStackTrace(t *testing.T) {
	err := New("foo")
	err = WithStack(err)
	err = WithStack(err)
	stack := InspectStackTrace(err)
	want := regexp.MustCompile(`^\[\]errors\.Frame{stack_test.go:97,`)
	if got := fmt.Sprintf("%#v", stack); !want.MatchString(got) {
		t.Errorf("Unexpected output: %s", got)
	}

	t.Run("no stack", func(t *testing.T) {
		err = stderr.New("foo")
		stack := InspectStackTrace(err)
		if stack != nil {
			t.Errorf("expected nil stack")
		}
	})
}

func TestInspectDeepStackTrace(t *testing.T) {
	err := New("foo")
	err = WithStack(err)
	err = WithStack(err)
	stack := InspectDeepStackTrace(err)
	want := regexp.MustCompile(`^\[\]errors\.Frame{stack_test.go:114,`)
	if got := fmt.Sprintf("%#v", stack); !want.MatchString(got) {
		t.Errorf("Unexpected output: %s", got)
	}

	t.Run("no stack", func(t *testing.T) {
		err = stderr.New("foo")
		stack := InspectDeepStackTrace(err)
		if stack != nil {
			t.Errorf("expected nil stack")
		}
	})
	t.Run("shallow", func(t *testing.T) {
		err := Errorf("foo")
		stack := InspectDeepStackTrace(err)
		if stack == nil {
			t.Errorf("expected a stack trace")
		}
	})
}
